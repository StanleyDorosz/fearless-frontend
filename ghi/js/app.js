function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    return `
      <div class="col-4">
      <div class="card shadow mx-1 mb-4">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-body-secondary">
          ${startDate.toDateString()} - ${endDate.toDateString()}
        </div>
      </div>
      </div>
    `;
  }

function displayError() {
  var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
  var alertTrigger = document.getElementById('liveAlertBtn')
  alertTrigger.style.display="block"
  function alert(message, type) {
    var wrapper = document.createElement('div')
    wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

    alertPlaceholder.append(wrapper)
  }

  if (alertTrigger) {
    alertTrigger.addEventListener('click', function () {
      alert('There was an issue fetching the data', 'failure')
    })
  }
}

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        displayError();
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts);
            const endDate = new Date(details.conference.ends);
            const locationName = (details.conference.location.name);
            const html = createCard(title, description, pictureUrl, startDate, endDate, locationName);
            const row = document.querySelector('.row');
            row.innerHTML += html;
          }
        }

      }
    } catch (error) {
      displayError();
      // Figure out what to do if an error is raised
    }

  });


// could've just done console.error as well instead of creating whole new function for displayError()

// new Date allows you to format date and there are different ones as well

// added div class col-4 because bootstrap has a 12 column layout 12/4 = 3 if changed to col-6 would have 2 column layout

// one class attribute can be added to any element and can add as many classes as you want as long as its separated by space

//

  // added await and async because inside function

// we are adding const data with an await as well as
// response.json() in order to convert the Readable stream data in the body
// into JSON

// added index of conference description went into conference
// then into description

// had to change console.log(details["conference"]) to a variable to reuse

// fixed undefined on document/page by adding $ signs to front of
// parameter imports and fixed this line
// const title = details.conference.name; before it was
// const title = details.conference.title;

// added console.error to catch and changed it from e to error for
// readability


// -----------------------------

// old code before for loop added

// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//         const data = await response.json();

//         const conference = data.conferences[0];
//         const nameTag = document.querySelector('.card-title');
//         nameTag.innerHTML = conference.name;

//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);

//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           const description = details["conference"]["description"];
//           const descriptionTag = document.querySelector('.card-text')
//           descriptionTag.innerHTML = description

//           const imageTag = document.querySelector('.card-img-top')
//           imageTag.src = details.conference.location.picture_url;
//         }

//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

//   });
