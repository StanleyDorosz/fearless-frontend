import React, { useEffect, useState } from 'react';

// don't need props because already grabbing data in the function

function LocationForm() {
    const [states, setStates] = useState([]);
    // Set the useState hook to store "name" in the component's state,
    // with a default initial value of an empty string.
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');
    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleRoomCountChange = (event) => {
        const value = event.target.value;
        setRoomCount(value);
    }

    const handleCityChange = (event) => {
        const value = event.target.value;
        setCity(value);
    }

    const handleStateChange = (event) => {
        const value = event.target.value;
        setState(value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
            setStates(data.states);

          }
        }

      useEffect(() => {
        fetchData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = state;

        console.log(data);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
        const newLocation = await response.json();
        console.log(newLocation);

        setName('');
        setRoomCount('');
        setCity('');
        setState('');
        };
      };

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new location</h1>
                        <form onSubmit={handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                            <input onChange={handleRoomCountChange} value={roomCount} placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control"/>
                            <label htmlFor="room_count">Room count</label>
                            </div>
                            <div className="form-floating mb-3">
                            <input onChange={handleCityChange} value={city} placeholder="City" required type="text" name="city" id="city" className="form-control"/>
                            <label htmlFor="city">City</label>
                            </div>
                            <div className="mb-3">
                            <select onChange={handleStateChange} value={state} required id="state" name="state" className="form-select">
                                <option value="">Choose a state</option>
                                {states.map(state => {
                                    return (
                                    <option key={state.abbreviation} value={state.abbreviation}>
                                        {state.name}
                                    </option>
                                    );
                                })}
                            </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default LocationForm;


//  Since the abbreviation property is guaranteed to be unique (see the State model class),
// you can safely use it as a value for the key attribute that React so badly wants.

// added key={state.abbreviation}
// to option tag


// following code to "loop over" the values
//  in the states array inside the JSX in the return method after the option tag.

// {states.map(state => {
    // return (
    //     <option value={state.abbreviation}>
    //       {state.name}
    //     </option>
    //   );
    // })}

    // added under option tag ^

// JSX is a syntax extension
// for JavaScript that lets you write HTML-like markup inside a JavaScript file

// took this out of if statement for locationForm() function

// const selectTag = document.getElementById('state');
// for (let state of data.states) {
//   const option = document.createElement('option');
//   option.value = state.abbreviation;
//   option.innerHTML = state.name;
//   selectTag.appendChild(option);




// we are creating a stateful component here (htmlFor location form)

// means that it can have internal data saved
// in the component when it gets created, just
// like our model classes in Django.


// All React components need a return method
// which returns a single HTML element as JSX


// grabs all of the HTML inside the div className="container"
// from new-location.html and pastes it over the p tag in
// the return method of the LocationForm component

// in React, everything has to have a closing tag.
// Or, you put this slash at the end of it." You show your
// teammate to change the > at the end of all three input tags to />.
// The red squiggly error indicators go away.

// changed all three input tags to have /> at end to get rid of errors


// Since our JSX gets turned into JavaScript,
// we can't use the word 'className' because that's a keyword in JavaScript.
// Instead, we have to use 'className

// changed all class to className


// I'd say it's the same kind of error.
// Just like 'class' is a keyword in JavaScript,
// so is 'for', like "for" in a for-loop. We need to
// change all of the 'for' attributes in our labels to 'htmlFor'

// Find & Replace operation to replace the for
// attributes with htmlFor attributes

// changed all for to have htmlFor


// last error was

// Use the `defaultValue` or `value` props on <select> instead
// of setting `selected` on <option>.

// Just remove the 'selected' attribute from the option tag in the HTML

// removed the selected attribute from the option tag in the html
