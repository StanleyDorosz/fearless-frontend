import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
    <Nav />
    <div className="container">
      <ConferenceForm />
      {/* <LocationForm /> */}
      {/* <AttendeesList attendees={props.attendees} /> */}
    </div>
    </>
  );
}

export default App;

// added an if statement because first time it runs
// it is undefined until it mounts
// this will prevent the mapping error (trying to map something undefined)



// added a react fragment <> above all tags and </> below all
// tags to prevent jsx error

// added bootstrap to make table striped
// make sure since it is JSX that add className not just class

// ex.  this wont work: <table class="table table-striped">

// this will work: <table className="table table-striped">

// added props as a parameter to the App function
// added a line of code inside the div tags
// code was : Number of attendees: {props.attendees.length}


// code before edits made is below for reference

// function App() {
//   return (
//     <div>
//     </div>
//   );
// }

// export default App;


// added an if statement at the beginning of the function

// because You see an error in the console and point to it.
// "It says that it can't
// read the length property of undefined. But
// I see the number on the screen."

// deleted number of attendees code :  Number of attendees: {props.attendees.length}
// from inside App function div


// changed code below to the code at the top

// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <div>
//     </div>
//   );
// }

// export default App;




// BEFORE :

// function App(props) {
//   return (
//     <div>
//       <table>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>

//         </tbody>
//       </table>
//     </div>
//   );
// }

// export default App;

//  AFTER : making it so we can loop through the array/list to put the names of
// attendees into our table on our localhost:3001 page

//  function App(props) {
//   return (
//     <div>
//       <table>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>
//             {props.attendees.map(attendee => {
//               return (
//                 <tr>
//                   <td>{ attendee.name }</td>
//                   <td>{ attendee.conference }</td>
//                 </tr>
//               );
//             })}
//         </tbody>
//       </table>
//     </div>
//   );
// }


//  added key={attendee.href}
// inside of tr tag
// because
// "This is one of those React things.
// Whenever you use the map method,
// you have to add a key so that React
// can keep track of the things it's generating.
//  If you add a key attribute to the tr tag and give
//  it a value unique to the attendee,
//  the error should go away."
