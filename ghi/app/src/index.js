import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  if (response.ok) {
    const data = await response.json();
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadAttendees();





// That's because React is using a
// tool called Babel to translate the JSX
//  files like this one into JavaScript.
//  So, your JavaScript is not yours,
//   it's Babel's."

// You look at the error message and think,
// Babel sucks. "I know how to fix this," you say.
// You change the code to look like this, wrapping
//  it in an async function.


// this code below throws an error because ^

// const response = await fetch('http://localhost:8001/api/attendees/');
// console.log(response);


// added code below to the loadAttendees function and deleted console.log(data) after
// const data variable

// root.render(
//   <React.StrictMode>
//     <App attendees={data.attendees} />
//   </React.StrictMode>
// );

// code after edits

// async function loadAttendees() {
//   const response = await fetch('http://localhost:8001/api/attendees/');
//   if (response.ok) {
//     const data = await response.json();
//     root.render(
//       <React.StrictMode>
//         <App attendees={data.attendees} />
//       </React.StrictMode>
//     );
//   } else {
//     console.error(response);
//   }
// }
// loadAttendees();
