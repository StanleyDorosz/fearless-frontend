import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);

    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [maximumPresentations, setMaximumPresentations] = useState('');
    const [maximumAttendees, setMaximumAttendees] = useState('');
    const [location, setLocation] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleStartChange = (event) => {
        const value = event.target.value;
        setStarts(value);
    }
    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnds(value);
    }
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }
    const handleMaximumPresentationsChange = (event) => {
        const value = event.target.value;
        setMaximumPresentations(value);
    }
    const handleMaximumAttendeesChange = (event) => {
        const value = event.target.value;
        setMaximumAttendees(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
            setLocations(data.locations);

          }
        }

      useEffect(() => {
        fetchData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maximumPresentations;
        data.max_attendees = maximumAttendees;
        data.location = location;

        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
        const newConference = await response.json();
        console.log(newConference);

        setName('');
        setStarts('');
        setEnds('');
        setDescription('');
        setMaximumPresentations('');
        setMaximumAttendees('');
        setLocation('');
        };
        const formTag = document.getElementById("create-conference-form");
        formTag.reset()
      };





    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Conference</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleStartChange} value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
                  <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEndChange} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
                  <label htmlFor="ends">Ends</label>
                </div>
                <div className="form-floating mb-3">
                  <textarea onChange={handleDescriptionChange} value={description} placeholder="Description" required name="description" id="description" className="form-control"/>
                  <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleMaximumPresentationsChange} value={maximumPresentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                  <label htmlFor="max_presentations">Maximum presentations</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleMaximumAttendeesChange} value={maximumAttendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                  <label htmlFor="max_attendees">Maximum attendees</label>
                </div>
                <div className="mb-3">
                  <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                                    return (
                                    <option key={location.id} value={location.id}>
                                        {location.name}
                                    </option>
                                    );
                                })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm



// dont forget to add Name to end of class ex. className

// for dates type="date"

// find required data and decide type of data
// based on models and insomnia create for that model to help

// placeholder is just what user will see in field box before typing anything

// id is usually the same as the name ^ only time different
// is if name is taken by another id

// dont forget the for label it will be identical to the  input name

// className= is just bootstrap can use others , based on preference or needs
//  or if using css can make your own

// remember to put closing tag slashes when going from regular html to JSX (only needed for certain tags)

// look at models to change input tags to match as well as ids

// when creating a new component (piece of page with JSX/React/JavaScript)

// step 1. create new js file

// step 2. create function that handles creating the "html" along with its functionality


// bare minimum to create a component below

// function ConferenceForm() {

//     return (
//         <p>on conference form</p>
//     )
// }

// export default ConferenceForm
